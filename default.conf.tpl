server {
    listen ${LISTEN_PORT};

    # serving static files through NGINX

    location /static {
        alias /vol/static;
    }

    # catch all the requests, uwsgi service will run a python application
    location / {
        uwsgi_pass              ${APP_HOST}:${APP_PORT};
        include                 /etc/nginx/uwsgi_params;
        client_max_body_size    10M;
    }
}