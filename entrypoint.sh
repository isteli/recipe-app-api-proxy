#!/bin/sh

# returns the error on the screen
set -e
# we are replacing the env files from tempalte default.conf.tpl and copying it to conf.d
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# start nginx to run in our foreground execution
nginx -g 'daemon off;'